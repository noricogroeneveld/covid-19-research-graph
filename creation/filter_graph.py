import logging
import pandas as pd

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%H:%M:%S')
logger = logging.getLogger('create_basic_graph.py')
logger.setLevel(logging.DEBUG)


def nodes_in_dataset(nodes):
    outside = nodes['type'] == 'outside'
    return nodes[~outside]


def add_reference_counts(nodes, edges):
    counts = edges.groupby('referencedTitle').agg('count')
    counts = counts.reset_index()
    counts = counts.rename(columns={'title': 'times_referenced',
                                    'referencedTitle': 'title'})
    counts['times_referenced'] = counts['times_referenced'].astype('Int32')
    nodes = nodes.merge(counts, on='title', how='left')
    return nodes


def referenced_nodes(nodes, threshold):
    return nodes[nodes['times_referenced'] > threshold]


def filter_edges(filtered_nodes, edges):
    present = pd.DataFrame(filtered_nodes['title'])
    present['present'] = True

    # remove edges if title is not in filtered_nodes
    edges = edges.merge(present, on='title', how='inner')
    edges = edges[edges['present']]
    edges = edges.drop(columns=['present'])

    # remove edges if referencedTitle is not in filtered_nodes
    present = present.rename(columns={'title': 'referencedTitle'})
    edges = edges.merge(present, on='referencedTitle', how='inner')
    edges = edges[edges['present']]
    edges = edges.drop(columns=['present'])

    return edges


def filter_graph(nodes, edges, in_dataset, threshold):
    if in_dataset:
        nodes = nodes_in_dataset(nodes)
    nodes = add_reference_counts(nodes, edges)
    nodes = referenced_nodes(nodes, threshold)

    edges = filter_edges(nodes, edges)
    return nodes, edges
