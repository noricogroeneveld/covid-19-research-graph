import logging
import pandas as pd
from creation.load_json import load_json
from creation.load_metadata import load_metadata, add_best_json_paths


logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%H:%M:%S')
logger = logging.getLogger('create_basic_graph.py')
logger.setLevel(logging.DEBUG)


def nodes_from_metadata(metadata):
    titles = metadata.loc[metadata['best_parse'] == 'none', 'title']
    titles = titles[~pd.isna(titles)]
    titles = titles_to_dataframe(titles, 'no_parse')
    return titles


def title_from_paper(metadata_row, json_data):
    paper_title = getattr(metadata_row, 'title')
    if paper_title is None or paper_title == '':
        paper_title = json_data['metadata']['title']
    if paper_title is None or paper_title == '':
        paper_title = getattr(metadata_row, 'cord_uid')
    return paper_title


def titles_to_dataframe(titles, paper_type):
    df = pd.DataFrame(titles, columns=['title', 'publish_time'])
    df['type'] = paper_type
    return df


def title_set_to_dataframe(title_set, paper_type):
    return titles_to_dataframe(list(title_set), paper_type)


def nodes_and_edges_from_json(metadata):
    titles_pdf_parse = set()
    titles_pmc_parse = set()
    titles_outside = set()
    references = set()

    # handle papers with a json file
    with_json = metadata[metadata['best_path'] != '']
    for row in with_json.itertuples():
        # read paper
        paper_path = getattr(row, 'best_path')
        paper = load_json(paper_path)

        # get and store title
        paper_title = title_from_paper(row, paper)
        paper_publish_time = getattr(row, 'publish_time')
        paper_data = (paper_title, paper_publish_time)
        paper_parse = getattr(row, 'best_parse')
        if paper_parse == 'pdf_json':
            titles_pdf_parse.add(paper_data)
        elif paper_parse == 'pmc_json':
            titles_pmc_parse.add(paper_data)
        else:
            logging.error(f'paper {paper_title} has no valid parse, but instead: {paper_parse}')

        # store references (as tuples in set) and possible new titles
        for reference in paper['bib_entries']:
            ref_title = paper['bib_entries'][reference]['title']
            ref_data = (ref_title, '')

            if ref_title != paper_title:
                references.add((paper_title, ref_title))
                titles_outside.add(ref_data)

    # create dataframe with titles of various types
    titles_pdf_parse = title_set_to_dataframe(titles_pdf_parse, 'pdf_parse')
    titles_pmc_parse = title_set_to_dataframe(titles_pmc_parse, 'pmc_parse')
    titles_outside = title_set_to_dataframe(titles_outside, 'outside')
    titles = pd.concat([titles_pdf_parse, titles_pmc_parse, titles_outside])

    references = pd.DataFrame(references, columns=['title', 'referencedTitle'])
    return titles, references


def load_graph():
    metadata = load_metadata()
    metadata = add_best_json_paths(metadata)
    meta_nodes = nodes_from_metadata(metadata)
    logger.info('Metadata nodes extracted and paper paths added')

    json_nodes, edges = nodes_and_edges_from_json(metadata)
    logger.info('Json files nodes and edges extracted')

    return meta_nodes, json_nodes, edges
