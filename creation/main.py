from creation.load_graph import load_graph
from creation.clean_graph import clean_graph
from creation.filter_graph import filter_graph
from creation.save_graph import save_graph


def main():
    meta_nodes, json_nodes, edges = load_graph()
    all_nodes, edges = clean_graph(meta_nodes, json_nodes, edges)
    save_graph(all_nodes, edges)

    subset_nodes, subset_edges = filter_graph(all_nodes, edges, True, 2)
    save_graph(subset_nodes, subset_edges, '_dataset_2')

    subset_nodes, subset_edges = filter_graph(all_nodes, edges, True, 5)
    save_graph(subset_nodes, subset_edges, '_dataset_5')

    subset_nodes, subset_edges = filter_graph(all_nodes, edges, False, 2)
    save_graph(subset_nodes, subset_edges, '_2')


if __name__ == '__main__':
    main()
