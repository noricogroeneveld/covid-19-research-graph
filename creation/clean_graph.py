import logging
import pandas as pd


REGEX_REMOVE = '[' + ':_()\n' + '\\\\' + '’\'“”\"' + ']'
REGEX_DASH = ' ?[—–-] ?'


logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%H:%M:%S')
logger = logging.getLogger('create_basic_graph.py')
logger.setLevel(logging.DEBUG)


def clean_title_series(title):
    title = title\
        .str.lower()\
        .str.replace(REGEX_REMOVE, '')\
        .str.replace(REGEX_DASH, '-')
    return title


def clean_edge_titles(edges):
    edges['title'] = clean_title_series(edges['title'])
    edges['referencedTitle'] = clean_title_series(edges['referencedTitle'])
    edges = edges[edges['title'] != edges['referencedTitle']]
    return edges


def deduplicate_nodes(all_nodes):
    columns = all_nodes.columns.values
    if len(columns) > 3 or 'title' not in columns or 'type' not in columns:
        logging.warning('Columns of dataframe all_nodes might not be correct anymore for function deduplicate_nodes')

    # remove complete duplicates
    all_nodes = all_nodes.drop_duplicates(subset=['title', 'type'])

    # remove duplicates for titles with multiple types
    # keep the row with the higher quality type
    # the highest quality is 'pmc_parse', so it's never removed
    type_order = ['outside', 'no_parse', 'pdf_parse']
    for worse_type in type_order:
        with_multiple_types = all_nodes.duplicated(subset=['title'], keep=False)
        is_worse_type = all_nodes['type'] == worse_type
        remove = with_multiple_types & is_worse_type
        all_nodes = all_nodes[~remove]

    return all_nodes


def index_of_na_or_empty(series):
    return (pd.isna(series)) | (series == '')


def edges_not_na_or_empty(edges, columns):
    for column in columns:
        na_or_empty = index_of_na_or_empty(edges[column])
        edges = edges[~na_or_empty]
    return edges


def log_counts(meta_nodes, json_nodes, edges, all_nodes):
    count_width = 7
    logger.debug(f'{meta_nodes.shape[0]:>{count_width}} meta nodes')
    logger.debug(f'{json_nodes.shape[0]:>{count_width}} json nodes')
    logger.debug(f'{len(meta_nodes["title"].unique()):>{count_width}} unique meta nodes')
    logger.debug(f'{len(json_nodes["title"].unique()):>{count_width}} unique json nodes')
    logger.debug(f'{all_nodes.shape[0]:>{count_width}} combined nodes')
    logger.debug(f'{edges.shape[0]:>{count_width}} edges')


def verify_graph(nodes, edges):
    # no duplicate nodes
    duplicate_count = nodes.duplicated(subset=['title'], keep='first').sum()
    if duplicate_count > 0:
        logger.warning(f'There are {duplicate_count} duplicate titles in the nodes')

    # no empty titles
    def check_empty_titles(series, name):
        empty_count = index_of_na_or_empty(series).sum()
        empty_count2 = ((pd.isna(series)) | (series == '')).sum()  # debug

        if empty_count2 != empty_count:  # debug
            print('Counts are different:', empty_count, empty_count2)  # debug

        if empty_count > 0:
            logger.warning(f'There are {empty_count} empty titles in {name}')

    check_empty_titles(nodes['title'], 'the nodes')
    check_empty_titles(edges['title'], 'edges from')
    check_empty_titles(edges['referencedTitle'], 'edges to')


def clean_graph(meta_nodes, json_nodes, edges):
    meta_nodes['original_title'] = meta_nodes['title']
    json_nodes['original_title'] = json_nodes['title']

    meta_nodes['title'] = clean_title_series(meta_nodes['title'])
    json_nodes['title'] = clean_title_series(json_nodes['title'])
    edges = clean_edge_titles(edges)
    logger.info('Titles cleaned')

    all_nodes = pd.concat([meta_nodes, json_nodes])
    all_nodes = all_nodes[~index_of_na_or_empty(all_nodes['title'])]
    all_nodes = deduplicate_nodes(all_nodes)

    edges = edges_not_na_or_empty(edges, ['title', 'referencedTitle'])
    edges = edges.drop_duplicates(subset=['title', 'referencedTitle'])

    log_counts(meta_nodes, json_nodes, edges, all_nodes)
    verify_graph(all_nodes, edges)

    return all_nodes, edges
