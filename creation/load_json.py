import os
import ujson


INPUT_PATH = './data/CORD-19-research-challenge/'
INPUT_FOLDERS = ['biorxiv_medrxiv', 'comm_use_subset',
                'custom_license', 'noncomm_use_subset']


def get_all_json_paths():
    return get_all_json_paths_for_parse('pdf_json') + \
           get_all_json_paths_for_parse('pmc_json')


def get_all_json_paths_for_parse(parse):
    data_paths = [INPUT_PATH + folder + '/' + folder + '/' + parse
                  for folder in INPUT_FOLDERS]
    json_paths = []
    for folder_path in data_paths:
        if os.path.isdir(folder_path):
            json_paths += get_file_paths_from_folder(folder_path)

    return json_paths


def get_file_paths_from_folder(folder_path):
    files = os.listdir(folder_path)
    files = [folder_path + '/' + x for x in files]
    return files


def load_json(path):
    with open(path) as f:
        data = ujson.load(f)
        return data


def print_json(data):
    print(ujson.dumps(data, indent=4))
