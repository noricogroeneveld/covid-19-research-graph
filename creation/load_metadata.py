import pandas as pd


INPUT_PATH = './data/CORD-19-research-challenge/'


def load_metadata():
    metadata = pd.read_csv(INPUT_PATH + 'metadata.csv')
    return metadata


def print_parse_counts(metadata):
    group_columns = ['has_pdf_parse', 'has_pmc_xml_parse']
    select_columns = group_columns + ['cord_uid']
    counts = metadata[select_columns].groupby(group_columns).count()
    counts = counts.rename(columns={'cord_uid': 'count'})
    print(counts)
    print()
    print(metadata['best_parse'].value_counts())


def add_best_json_paths(metadata):
    metadata['best_path'] = ''

    metadata['best_parse'] = 'none'
    metadata.loc[metadata['has_pdf_parse'], 'best_parse'] = 'pdf_json'
    metadata.loc[metadata['has_pmc_xml_parse'], 'best_parse'] = 'pmc_json'

    # deal with duplicate pdf hashes
    duplicate_pdf = metadata['sha'].str.contains('; ', regex=False) == True
    first_pdf = metadata.loc[duplicate_pdf, 'sha'].str.split('; ').str[0]
    metadata['first_pdf'] = metadata['sha']
    metadata.loc[duplicate_pdf, 'first_pdf'] = first_pdf

    # set folder paths for rows with any parse
    mask = metadata['best_parse'] != 'none'
    metadata.loc[mask, 'best_path'] =\
        INPUT_PATH +\
        metadata.loc[mask, 'full_text_file'] + '/' +\
        metadata.loc[mask, 'full_text_file'] + '/' + \
        metadata.loc[mask, 'best_parse'] + '/'

    # add pdf filename to paths
    mask = (metadata['best_parse'] == 'pdf_json')
    metadata.loc[mask, 'best_path'] += metadata.loc[mask, 'first_pdf'] + '.json'

    # add pmc filename to paths
    mask = metadata['best_parse'] == 'pmc_json'
    metadata.loc[mask, 'best_path'] += metadata.loc[mask, 'pmcid'] + '.xml.json'

    metadata = metadata.drop(columns=['first_pdf'])
    return metadata
