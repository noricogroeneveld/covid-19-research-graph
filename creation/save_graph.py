import logging


OUTPUT_PATH = './data/graphs/'

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%H:%M:%S')
logger = logging.getLogger('create_basic_graph.py')
logger.setLevel(logging.DEBUG)


def save_to_csv(df, name, header, label):
    name += label
    if df is not None:
        path = f'{OUTPUT_PATH}{name}.csv'
        df.to_csv(path, index=False, header=header)
        logger.info(f'saved file {name}.csv with shape {df.shape}')
    else:
        logger.debug(f'Did not save file {name}.csv, because the df was None')


def save_for_neo4j(nodes, edges, label):
    nodes = nodes.drop(columns=['original_title'])
    save_to_csv(nodes, 'neo_nodes', False, label)
    save_to_csv(edges, 'neo_edges', False, label)


def save_for_gephi(nodes, edges, label):
    nodes = nodes.rename(columns={'title': 'id'})
    edges = edges.rename(columns={'title': 'source', 'referencedTitle': 'target'})
    save_to_csv(nodes, 'gephi_nodes', True, label)
    save_to_csv(edges, 'gephi_edges', True, label)


def save_graph(nodes, edges, label=''):
    save_for_neo4j(nodes, edges, label)
    save_for_gephi(nodes, edges, label)
