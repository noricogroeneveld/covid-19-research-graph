{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Network of papers and references\n",
    "\n",
    "Papers reference other papers, and together they form a network. This network contains various kinds of information. For example, it can be used to find the most influential papers. Or when looking at a specific paper, you can find papers that indirectly reference it or are indirectly referenced by it. When looking at multiple papers, you can see how they are connected. More complex information can also be extracted.\n",
    "\n",
    "About two thirds of the papers in the dataset have json files that include the references. This notebook extracts those references and saves the network. It also shows two examples of how the network can be used in other tools.\n",
    "\n",
    "The network is a graph with the papers as nodes and the references as directed edges. The papers are identified by their titles, and the references are identified by a pair of titles. The network can be imported in graph tools such as Neo4j or Gephi, which can be used to query various aspects of the network and visualise it.\n",
    "\n",
    "The repository with the code for this project can be found on [GitLab](https://gitlab.com/norico.groeneveld/covid-19-research-graph).\n",
    "\n",
    "You can also go straight to [the visualisation](https://noricogroeneveld.gitlab.io/covid-19-research-visualisation/)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "from datetime import datetime\n",
    "import os\n",
    "import pandas as pd\n",
    "import ujson\n",
    "\n",
    "# paths for Kaggle\n",
    "# INPUT_PATH = '../input/CORD-19-research-challenge/'\n",
    "# OUTPUT_PATH = ''\n",
    "\n",
    "# paths for repository\n",
    "INPUT_PATH = '../data/CORD-19-research-challenge/'\n",
    "OUTPUT_PATH = '../data/graphs/'\n",
    "\n",
    "# function to log status with time since start\n",
    "start_time = datetime.now()\n",
    "def print_status(message):\n",
    "    seconds = (datetime.now() - start_time).seconds\n",
    "    minutes = seconds // 60\n",
    "    seconds = seconds - (minutes * 60)\n",
    "    print(f'{minutes:02}:{seconds:02} - {message}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Loading\n",
    "In order to add the references to the graph, we need to read the json files. However, not all papers have a json file, and some papers have two. When there are two json files, the one that is parsed from xml is preferred over the one parsed from pdf. We add two columns to the metadata dataframe. One is the best available parse, the other is the path to the best available json file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "00:01 - loaded metadata and added json paths\n",
      "                                 count\n",
      "has_pdf_parse has_pmc_xml_parse       \n",
      "False         False              12718\n",
      "              True                1108\n",
      "True          False              22385\n",
      "              True               21155\n",
      "\n",
      "pdf_json    22385\n",
      "pmc_json    22263\n",
      "none        12718\n",
      "Name: best_parse, dtype: int64\n"
     ]
    }
   ],
   "source": [
    "def load_metadata():\n",
    "    metadata = pd.read_csv(INPUT_PATH + 'metadata.csv')\n",
    "    return metadata\n",
    "\n",
    "\n",
    "def print_parse_counts(metadata):\n",
    "    group_columns = ['has_pdf_parse', 'has_pmc_xml_parse']\n",
    "    select_columns = group_columns + ['cord_uid']\n",
    "    counts = metadata[select_columns].groupby(group_columns).count()\n",
    "    counts = counts.rename(columns={'cord_uid': 'count'})\n",
    "    print(counts)\n",
    "    print()\n",
    "    print(metadata['best_parse'].value_counts())\n",
    "\n",
    "\n",
    "def add_best_json_paths(metadata):\n",
    "    metadata['best_path'] = ''\n",
    "\n",
    "    metadata['best_parse'] = 'none'\n",
    "    metadata.loc[metadata['has_pdf_parse'], 'best_parse'] = 'pdf_json'\n",
    "    metadata.loc[metadata['has_pmc_xml_parse'], 'best_parse'] = 'pmc_json'\n",
    "\n",
    "    # deal with duplicate pdf hashes\n",
    "    duplicate_pdf = metadata['sha'].str.contains('; ', regex=False) == True\n",
    "    first_pdf = metadata.loc[duplicate_pdf, 'sha'].str.split('; ').str[0]\n",
    "    metadata['first_pdf'] = metadata['sha']\n",
    "    metadata.loc[duplicate_pdf, 'first_pdf'] = first_pdf\n",
    "    \n",
    "    # set folder paths for rows with any parse\n",
    "    mask = metadata['best_parse'] != 'none'\n",
    "    metadata.loc[mask, 'best_path'] =\\\n",
    "        INPUT_PATH +\\\n",
    "        metadata.loc[mask, 'full_text_file'] + '/' +\\\n",
    "        metadata.loc[mask, 'full_text_file'] + '/' +\\\n",
    "        metadata.loc[mask, 'best_parse'] + '/'\n",
    "\n",
    "    # add pdf filename to paths\n",
    "    mask = (metadata['best_parse'] == 'pdf_json')\n",
    "    metadata.loc[mask, 'best_path'] += metadata.loc[mask, 'first_pdf'] + '.json'\n",
    "\n",
    "    # add pmc filename to paths\n",
    "    mask = metadata['best_parse'] == 'pmc_json'\n",
    "    metadata.loc[mask, 'best_path'] += metadata.loc[mask, 'pmcid'] + '.xml.json'\n",
    "\n",
    "    metadata = metadata.drop(columns=['first_pdf'])\n",
    "    return metadata\n",
    "\n",
    "\n",
    "metadata = load_metadata()\n",
    "metadata = add_best_json_paths(metadata)\n",
    "print_status('loaded metadata and added json paths')\n",
    "print_parse_counts(metadata)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The graph information we can get from purely the metadata.csv file is just the titles. Because the papers with a parse will be added later, we only need the papers without a parse."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "00:01 - extracted 12593 nodes from metadata\n"
     ]
    }
   ],
   "source": [
    "def titles_to_dataframe(titles, paper_type):\n",
    "    df = pd.DataFrame(titles, columns=['title', 'publish_time'])\n",
    "    df['type'] = paper_type\n",
    "    return df\n",
    "\n",
    "\n",
    "def nodes_from_metadata(metadata):\n",
    "    titles = metadata.loc[metadata['best_parse'] == 'none', 'title']\n",
    "    titles = titles[~pd.isna(titles)]\n",
    "    titles = titles_to_dataframe(titles, 'no_parse')\n",
    "    return titles\n",
    "\n",
    "\n",
    "meta_nodes = nodes_from_metadata(metadata)\n",
    "print_status(f'extracted {meta_nodes.shape[0]} nodes from metadata')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we actually load the json files, we need some helper functions. Because the title in the json file is not always clean, the title from the metadata file is preferred. When there is no title in either of those, we add an id that is always present, so the references can still be counted."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "def load_json(path):\n",
    "    with open(path) as f:\n",
    "        data = ujson.load(f)\n",
    "        return data\n",
    "\n",
    "    \n",
    "def title_from_paper(metadata_row, json_data):\n",
    "    paper_title = getattr(metadata_row, 'title')\n",
    "    if paper_title is None or paper_title == '':\n",
    "        paper_title = json_data['metadata']['title']\n",
    "    if paper_title is None or paper_title == '':\n",
    "        paper_title = getattr(metadata_row, 'cord_uid')\n",
    "    return paper_title\n",
    "\n",
    "\n",
    "def title_set_to_dataframe(title_set, paper_type):\n",
    "    return titles_to_dataframe(list(title_set), paper_type)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can load the nodes and edges from the json files. In order to filter the papers later, we keep track of the parse."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "02:09 - loaded 1427013 nodes and 2222598 edges from json files\n"
     ]
    }
   ],
   "source": [
    "def nodes_and_edges_from_json(metadata):\n",
    "    titles_pdf_parse = set()\n",
    "    titles_pmc_parse = set()\n",
    "    titles_outside = set()\n",
    "    references = set()\n",
    "\n",
    "    # handle papers with a json file\n",
    "    with_json = metadata[metadata['best_path'] != '']\n",
    "    for row in with_json.itertuples():\n",
    "        # read paper\n",
    "        paper_path = getattr(row, 'best_path')\n",
    "        paper = load_json(paper_path)\n",
    "\n",
    "        # get and store title\n",
    "        paper_title = title_from_paper(row, paper)\n",
    "        paper_publish_time = getattr(row, 'publish_time')\n",
    "        paper_data = (paper_title, paper_publish_time)\n",
    "        paper_parse = getattr(row, 'best_parse')\n",
    "        if paper_parse == 'pdf_json':\n",
    "            titles_pdf_parse.add(paper_data)\n",
    "        elif paper_parse == 'pmc_json':\n",
    "            titles_pmc_parse.add(paper_data)\n",
    "\n",
    "        # store references (as tuples in set) and possible new titles\n",
    "        for reference in paper['bib_entries']:\n",
    "            ref_title = paper['bib_entries'][reference]['title']\n",
    "            ref_data = (ref_title, '')\n",
    "\n",
    "            if ref_title != paper_title:\n",
    "                references.add((paper_title, ref_title))\n",
    "                titles_outside.add(ref_data)\n",
    "\n",
    "    # create dataframe with titles of various types\n",
    "    titles_pdf_parse = title_set_to_dataframe(titles_pdf_parse, 'pdf_parse')\n",
    "    titles_pmc_parse = title_set_to_dataframe(titles_pmc_parse, 'pmc_parse')\n",
    "    titles_outside = title_set_to_dataframe(titles_outside, 'outside')\n",
    "    titles = pd.concat([titles_pdf_parse, titles_pmc_parse, titles_outside])\n",
    "\n",
    "    references = pd.DataFrame(references, columns=['title', 'referencedTitle'])\n",
    "    return titles, references\n",
    "\n",
    "\n",
    "json_nodes, edges = nodes_and_edges_from_json(metadata)\n",
    "print_status(f'loaded {json_nodes.shape[0]} nodes and {edges.shape[0]} edges from json files')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Cleaning\n",
    "The titles sometimes contain characters that result in mismatches or may cause problems later (such as \\ and quotes), so we want to remove those. Because various kinds of dashes are used, we replace them with the regular dash."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "02:59 - cleaned titles\n"
     ]
    }
   ],
   "source": [
    "REGEX_REMOVE = '[' + ':_()\\n' + '\\\\\\\\' + '’\\'“”\\\"' + ']'\n",
    "REGEX_DASH = ' ?[—–-] ?'\n",
    "\n",
    "def clean_title_series(title):\n",
    "    title = title\\\n",
    "        .str.lower()\\\n",
    "        .str.replace(REGEX_REMOVE, '')\\\n",
    "        .str.replace(REGEX_DASH, '-')\n",
    "    return title\n",
    "\n",
    "\n",
    "def clean_edge_titles(edges):\n",
    "    edges['title'] = clean_title_series(edges['title'])\n",
    "    edges['referencedTitle'] = clean_title_series(edges['referencedTitle'])\n",
    "    edges = edges[edges['title'] != edges['referencedTitle']]\n",
    "    return edges\n",
    "\n",
    "meta_nodes['original_title'] = meta_nodes['title']\n",
    "json_nodes['original_title'] = json_nodes['title']\n",
    "meta_nodes['title'] = clean_title_series(meta_nodes['title'])\n",
    "json_nodes['title'] = clean_title_series(json_nodes['title'])\n",
    "edges = clean_edge_titles(edges)\n",
    "print_status('cleaned titles')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we need to combine the two dataframes with nodes, after which we can remove duplicates and empty titles. When the title has been added with different parses, we want to keep the parse with the highest quality."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "def deduplicate_nodes(all_nodes):\n",
    "    # remove complete duplicates\n",
    "    all_nodes = all_nodes.drop_duplicates(subset=['title', 'type'])\n",
    "\n",
    "    # remove duplicates for titles with multiple types\n",
    "    # keep the row with the higher quality type\n",
    "    # the highest quality is 'pmc_parse', so it's never removed\n",
    "    type_order = ['outside', 'no_parse', 'pdf_parse']\n",
    "    for worse_type in type_order:\n",
    "        with_multiple_types = all_nodes.duplicated(subset=['title'], keep=False)\n",
    "        is_worse_type = all_nodes['type'] == worse_type\n",
    "        remove = with_multiple_types & is_worse_type\n",
    "        all_nodes = all_nodes[~remove]\n",
    "\n",
    "    return all_nodes\n",
    "\n",
    "\n",
    "def index_of_na_or_empty(series):\n",
    "    return (pd.isna(series)) | (series == '')\n",
    "\n",
    "\n",
    "all_nodes = pd.concat([meta_nodes, json_nodes])\n",
    "all_nodes = all_nodes[~index_of_na_or_empty(all_nodes['title'])]\n",
    "all_nodes = deduplicate_nodes(all_nodes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also need to remove empty titles and duplicates from the edges."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "def edges_not_na_or_empty(edges, columns):\n",
    "    for column in columns:\n",
    "        na_or_empty = index_of_na_or_empty(edges[column])\n",
    "        edges = edges[~na_or_empty]\n",
    "    return edges\n",
    "\n",
    "\n",
    "edges = edges_not_na_or_empty(edges, ['title', 'referencedTitle'])\n",
    "edges = edges.drop_duplicates(subset=['title', 'referencedTitle'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Filtering\n",
    "Large graphs are hard to visualise and are resource-intensive. For some cases, it's better to work with only part of the graph. Here we only use nodes that are referenced at least twice, and remove any edges outside of these nodes. The code can be used with other threshold too, and to filter out papers outside the dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "def nodes_in_dataset(nodes):\n",
    "    outside = nodes['type'] == 'outside'\n",
    "    return nodes[~outside]\n",
    "\n",
    "\n",
    "def add_reference_counts(nodes, edges):\n",
    "    counts = edges.groupby('referencedTitle').agg('count')\n",
    "    counts = counts.reset_index()\n",
    "    counts = counts.rename(columns={'title': 'times_referenced', 'referencedTitle': 'title'})\n",
    "    counts['times_referenced'] = counts['times_referenced'].astype('Int32')\n",
    "    nodes = nodes.merge(counts, on='title', how='left')\n",
    "    return nodes\n",
    "\n",
    "\n",
    "def referenced_nodes(nodes, threshold):\n",
    "    return nodes[nodes['times_referenced'] > threshold]\n",
    "\n",
    "\n",
    "def filter_edges(filtered_nodes, edges):\n",
    "    present = pd.DataFrame(filtered_nodes['title'])\n",
    "    present['present'] = True\n",
    "\n",
    "    # remove edges if title is not in filtered_nodes\n",
    "    edges = edges.merge(present, on='title', how='inner')\n",
    "    edges = edges[edges['present']]\n",
    "    edges = edges.drop(columns=['present'])\n",
    "\n",
    "    # remove edges if referencedTitle is not in filtered_nodes\n",
    "    present = present.rename(columns={'title': 'referencedTitle'})\n",
    "    edges = edges.merge(present, on='referencedTitle', how='inner')\n",
    "    edges = edges[edges['present']]\n",
    "    edges = edges.drop(columns=['present'])\n",
    "\n",
    "    return edges\n",
    "\n",
    "\n",
    "def filter_graph(nodes, edges, in_dataset, threshold):\n",
    "    if in_dataset:\n",
    "        nodes = nodes_in_dataset(nodes)\n",
    "    nodes = add_reference_counts(nodes, edges)\n",
    "    nodes = referenced_nodes(nodes, threshold)\n",
    "\n",
    "    edges = filter_edges(nodes, edges)\n",
    "    return nodes, edges\n",
    "\n",
    "# exclude papers that are only referenced once\n",
    "subset_nodes, subset_edges = filter_graph(all_nodes, edges, False, 2)\n",
    "\n",
    "# exclude papers that are referenced less than 5 times, or are outside the original dataset\n",
    "subsubset_nodes, subsubset_edges = filter_graph(all_nodes, edges, True, 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Saving"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "03:37 - saved file neo_nodes.csv with shape (1328877, 3)\n",
      "03:50 - saved file neo_edges.csv with shape (2201909, 2)\n",
      "04:00 - saved file gephi_nodes.csv with shape (1328877, 4)\n",
      "04:12 - saved file gephi_edges.csv with shape (2201909, 2)\n",
      "04:13 - saved file neo_nodes_2.csv with shape (130386, 4)\n",
      "04:15 - saved file neo_edges_2.csv with shape (343667, 2)\n",
      "04:16 - saved file gephi_nodes_2.csv with shape (130386, 5)\n",
      "04:18 - saved file gephi_edges_2.csv with shape (343667, 2)\n",
      "04:18 - saved file neo_nodes_dataset_5.csv with shape (8977, 4)\n",
      "04:18 - saved file neo_edges_dataset_5.csv with shape (63490, 2)\n",
      "04:18 - saved file gephi_nodes_dataset_5.csv with shape (8977, 5)\n",
      "04:19 - saved file gephi_edges_dataset_5.csv with shape (63490, 2)\n",
      "04:19 - Congratulations, the notebook has finished!\n"
     ]
    }
   ],
   "source": [
    "def save_to_csv(df, name, header, label):\n",
    "    name += label\n",
    "    if df is not None:\n",
    "        path = f'{OUTPUT_PATH}{name}.csv'\n",
    "        df.to_csv(path, index=False, header=header)\n",
    "        print_status(f'saved file {name}.csv with shape {df.shape}')\n",
    "    else:\n",
    "        print_status(f'Did not save file {name}.csv, because the df was None')\n",
    "\n",
    "\n",
    "def save_for_neo4j(nodes, edges, label):\n",
    "    nodes = nodes.drop(columns=['original_title'])\n",
    "    save_to_csv(nodes, 'neo_nodes', False, label)\n",
    "    save_to_csv(edges, 'neo_edges', False, label)\n",
    "\n",
    "\n",
    "def save_for_gephi(nodes, edges, label):\n",
    "    nodes = nodes.rename(columns={'title': 'id'})\n",
    "    edges = edges.rename(columns={'title': 'source', 'referencedTitle': 'target'})\n",
    "    save_to_csv(nodes, 'gephi_nodes', True, label)\n",
    "    save_to_csv(edges, 'gephi_edges', True, label)\n",
    "\n",
    "\n",
    "def save_graph(nodes, edges, label=''):\n",
    "    save_for_neo4j(nodes, edges, label)\n",
    "    save_for_gephi(nodes, edges, label)\n",
    "\n",
    "\n",
    "# complete graph\n",
    "save_graph(all_nodes, edges)\n",
    "\n",
    "# filtered data used for large visualisation\n",
    "save_graph(subset_nodes, subset_edges, '_2')\n",
    "\n",
    "# filtered data used for normal visualisation\n",
    "save_graph(subsubset_nodes, subsubset_edges, '_dataset_5')\n",
    "\n",
    "print_status('Congratulations, the notebook has finished!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have csv files that contain the nodes and edges of the research graph. They are ready to be imported in Neo4j or Gephi for analysis or visualisation. Other tools can be used as well, with the same csv files or slightly modified ones.\n",
    "\n",
    "[Neo4j](https://neo4j.com/) is a graph database, which is useful for data with many-to-many relationships. It uses the Cypher Query Language and has a Browser to execute queries and view the results. [Gephi](https://gephi.org/) is a program for the visualisation of graphs. It has an interface that's not very complicated, though the number of options can be overwhelming.\n",
    "\n",
    "# Neo4j import\n",
    "Create a database and put the csv files in the corresponding import folder. Open Neo4j Browser and execute the following three Cypher queries. You might have to change the filenames.\n",
    "```\n",
    "CREATE CONSTRAINT ON (p:Paper) ASSERT p.title IS UNIQUE;\n",
    "\n",
    ":auto USING PERIODIC COMMIT\n",
    "LOAD CSV FROM 'file:///neo_nodes.csv' AS line\n",
    "CREATE (:Paper { title: line[0]});\n",
    "\n",
    ":auto USING PERIODIC COMMIT\n",
    "LOAD CSV FROM 'file:///neo_edges.csv' AS line\n",
    "MATCH (paper1:Paper { title: line[0]}),(paper2:Paper { title: line[1]})\n",
    "CREATE (paper1)-[:REFERENCES]->(paper2);\n",
    "```\n",
    "After that, all the data is loaded and ready to be queried. As an example, you can find the most referenced papers and the references between them.\n",
    "```\n",
    "MATCH ()-[r]->(n)\n",
    "WITH n, count(r) as relcnt\n",
    "WHERE relcnt > 200\n",
    "RETURN n, relcnt;\n",
    "```\n",
    "\n",
    "# Gephi import and visualisation\n",
    "* From the tools menu, make sure the following plugins are installed: Circle Pack, SigmaJS exporter.\n",
    "* From the file menu, open the csv file with nodes and follow the steps. Set the Graph Type to Directed.\n",
    "* After that, open the csv file with edges. The only change you need to make is to append it to the current workspace instead of creating a new one.\n",
    "* In the Statistics panel, run Modularity. There is no need to use weights, but set the Resolution 2 when you use many papers. The Modularity algorithm clusters the papers into highly connected communities.\n",
    "* In the Appearance panel, set the node color to Partition using Modularity Class, optionally change the colors for some classes, and apply it.\n",
    "* In the same panel, set the node size to Ranking using referenced, with size 5 to 50 (or 100 if using many papers), and apply it\n",
    "* In the Layout panel, select Circle Pack Layout, set Hierarchy1 to Modularity Class and run it.\n",
    "* In the Preview tab, set the Border With of Nodes to 0. Refresh to see the preview.\n",
    "* From the file menu, export the graph as Sigma.js template. Enable *Include search* and *Group edges by direction*, and set the *Group Selector* To *Modularity class*. Make sure that the option *Replace node ids with numbers* is disabled. \n",
    "* To view the result, run *python -m http.server* in the folder with the Sigma.js template. It can also be uploaded, for example to Github Pages or Gitlab Pages. The nodes are smaller than in Gephi, but you can increase the *maxNodeSize* property in the *config.json* file. \n",
    "\n",
    "\n",
    "In the exported visualisation, you can select a paper and view its properties and connections. It is also possible to search for a paper or select a modularity cluster.\n",
    "I created two exports, one with the papers in the dataset that are referenced at least 5 times, and another one with all papers (including those outside the dataset) that are referenced at least twice.\n",
    "Here are links to view the [first visualisation](https://noricogroeneveld.gitlab.io/covid-19-research-visualisation/) and the [larger second visualisation](https://noricogroeneveld.gitlab.io/covid-19-research-visualisation/full/). The large visualisation has a large data file, so it takes a while to load and is slower.\n",
    "\n",
    "Gephi can also be used to calculate properties of the graph, for example with the Pagerank algorithm.\n",
    "\n",
    "\n",
    "# Next steps\n",
    "* There are more ways to visualise or query the graph and gain information from it. There is research about research graph, which can help with that.\n",
    "* The visualisation can be improved, especially regarding the paper attributes.\n",
    "* There are various edge cases that can be dealt with in a better way. It is assumed that titles and references are unique, but this is probably not the case. Duplicate titles might belong to papers that are actually different, but now they are combined. There are also some titles that clearly aren't a normal title for a paper.\n",
    "* There is more information in the dataset. The most relevant are authors, laboratories, institutions and journals. They can be added to the network as new nodes and edges, or as extra data for the current nodes.\n",
    "* It is possible to extract the sentence that a reference is used in, to give more context about the relationship.\n",
    "* Other sources might have extra research on COVID-19 to add to the graph.\n",
    "\n",
    "\n",
    "# Related solutions\n",
    "For COVID-19 research, there are two related solutions. The [references-based atlas notebook](https://www.kaggle.com/mmoeller/a-references-based-atlas-of-covid-19-research) by Moritz Moeller extracts the references from PubMed and visualises them with Gephi. On [covid.curiosity.ai](https://covid.curiosity.ai/) you can use a graph-based search engine with various exploration features.\n",
    "\n",
    "There are also various projects related to research graphs in general, including: [Microsoft Academic Graph](https://www.microsoft.com/en-us/research/project/microsoft-academic-graph/), [OpenAIRE Research Graph](https://www.openaire.eu/blogs/the-openaire-research-graph), [ResearchGraph](https://researchgraph.org/), [Project Freya PID Graph](https://www.project-freya.eu/en/blogs/blogs/the-pid-graph) and the [Open Research Knowledge Graph](https://projects.tib.eu/orkg)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
