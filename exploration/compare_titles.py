import pandas as pd
from creation.clean_graph import clean_title_series
from creation.load_metadata import load_metadata
from creation.load_json import get_all_json_paths_for_parse, load_json


def nodes_from_metadata(metadata, meta_id):
    nodes = metadata[['title', meta_id]]
    return nodes


def nodes_from_json(json_paths, meta_id):
    node_tuples = []

    for json_path in json_paths:
        paper = load_json(json_path)
        paper_title = paper['metadata']['title']
        paper_id = paper['paper_id']

        node_tuples.append((paper_title, paper_id))

    nodes = pd.DataFrame(node_tuples, columns=['title_json', meta_id])
    return nodes


def compare_titles(parse, meta_id):
    print('loading data for parse', parse)
    metadata = load_metadata()
    json_paths = get_all_json_paths_for_parse(parse)

    meta_nodes = nodes_from_metadata(metadata, meta_id)
    json_nodes = nodes_from_json(json_paths, meta_id)

    combined = meta_nodes.merge(json_nodes, on=meta_id, how='outer')
    print(combined.shape, 'all titles')

    combined = combined[combined['title'] != combined['title_json']]
    print(combined.shape, 'different titles')

    combined = combined.dropna(subset=['title', 'title_json'])
    print(combined.shape, 'after nan filter')

    combined['title'] = clean_title_series(combined['title'])
    combined['title_json'] = clean_title_series(combined['title_json'])
    combined = combined[combined['title'] != combined['title_json']]
    print(combined.shape, 'after cleaning')

    # for row in combined.itertuples():
    #     print()
    #     print(getattr(row, 'title'))
    #     print(getattr(row, 'title_json'))


if __name__ == '__main__':
    compare_titles('pdf_json', 'sha')
    compare_titles('pmc_json', 'pmcid')
