import matplotlib.pyplot as plt
import pandas as pd
from creation.filter_graph import add_reference_counts

OUTPUT_PATH = './data/graphs/'


def load_nodes_and_edges(label):
    nodes_path = OUTPUT_PATH + 'gephi_nodes' + label + '.csv'
    edges_path = OUTPUT_PATH + 'gephi_edges' + label + '.csv'
    nodes = pd.read_csv(nodes_path)
    edges = pd.read_csv(edges_path)
    return nodes, edges


def analyse_nodes_and_edges(nodes, edges):
    nodes = nodes.rename(columns={'id': 'title'})
    edges = edges.rename(columns={'source': 'title',
                                  'target': 'referencedTitle'})

    if 'referenced' not in nodes.columns.values:
        nodes = add_reference_counts(nodes, edges)

    counts = nodes['referenced'].value_counts()
    counts = counts\
        .reset_index()\
        .rename(columns={'referenced': 'count',
                         'index': 'referenced'})\
        .sort_values('referenced')

    print('min', counts['referenced'].min())
    print('mean', counts['referenced'].mean())
    print('max', counts['referenced'].max())

    weighted = (counts['referenced'] * counts['count']).sum() / counts['count'].sum()
    print('weighted average', weighted)
    print(counts.head(10))
    print(counts.tail(10))
    print()


def analyse_graph():
    labels = ['', '_2', '_dataset_2']
    for label in labels:
        print('label', label)
        nodes, edges = load_nodes_and_edges(label)
        analyse_nodes_and_edges(nodes, edges)
    plt.show()


if __name__ == '__main__':
    analyse_graph()
