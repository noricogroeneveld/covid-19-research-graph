import hyperjson
import json
import rapidjson
import time
import ujson
from creation.load_json import get_all_json_paths


def benchmark(json_paths, option):
    name, read = option
    start = time.time()
    lengths = 0

    for path in json_paths:
        with open(path) as f:
            data = read(f)
            lengths += len(data['paper_id'])

    print(name, time.time() - start, lengths)


def run_benchmarks():
    json_paths = get_all_json_paths()
    options = [('python', json.load),
               ('ujson', ujson.load),
               ('hyperjson', hyperjson.load),
               ('rapidjson', rapidjson.load)]
    # order = [0, 1, 2, 3]
    order = [0, 1, 2, 3, 1, 0, 3, 2]

    for i in order:
        benchmark(json_paths, options[i])


if __name__ == '__main__':
    run_benchmarks()

# conclusion: ujson is slightly faster than json, but hyperjson and rapidjson are not
