import pandas as pd
import os
from creation.load_metadata import load_metadata


INPUT_PATH = './data/CORD-19-research-challenge/'


def print_file_counts(folder, parse):
    sub_folders = os.listdir(folder)
    for sub_folder in sub_folders:

        sub_path = folder + sub_folder
        if 'embeddings' in sub_path or not os.path.isdir(sub_path):
            continue

        sub_sub_folders = os.listdir(sub_path)
        for sub_sub_folder in sub_sub_folders:

            sub_sub_path = f'{sub_path}/{sub_sub_folder}/{parse}'
            try:
                count = len(os.listdir(sub_sub_path))
                print(count, 'in', sub_sub_path)
            except FileNotFoundError:
                print('0 because path', sub_sub_path, 'does not exist')


def metadata_stats():
    metadata = load_metadata()
    print(metadata.shape)
    print(pd.isna(metadata['title']).sum(), 'na')
    print((metadata['title'] == '').sum(), 'empty')
    print(len(metadata['title'].unique()), 'unique titles')
    print('occurrences per title')
    counts = metadata['title'].value_counts()
    print(counts.value_counts())
    print('most occurring titles')
    print(counts[counts > 4])
    # print(metadata[pd.isna(metadata['title'])].to_string())
    # print(metadata.describe(include='all').to_string())


def parse_stats():
    metadata = load_metadata()
    parse_columns = ['has_pdf_parse', 'has_pmc_xml_parse']
    counts = metadata\
        .groupby(parse_columns).size()\
        .reset_index()\
        .rename(columns={0: 'count'})
    print(counts)


if __name__ == '__main__':
    # print_file_counts(INPUT_PATH, 'pdf_json')
    # print_file_counts(INPUT_PATH, 'pmc_json')

    metadata_stats()
    # parse_stats()
