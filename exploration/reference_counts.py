from collections import Counter
from numpy import random
from statistics import mean
from creation.load_json import get_all_json_paths_for_parse, load_json


def title_is_missing(paper):
    if 'title' in paper and paper['title'] != '':
        return True
    else:
        return False


def missing_titles(parse):
    paths = get_all_json_paths_for_parse(parse)
    missing = [title_is_missing(load_json(path)) for path in paths]
    print('debug', min(missing))
    print(sum(missing), 'out of', len(missing), 'titles are missing for', parse)


def get_reference_count(paper):
    if 'bib_entries' in paper:
        return len(paper['bib_entries'])
    else:
        return 0


def get_all_reference_counts(parse):
    paths = get_all_json_paths_for_parse(parse)
    counts = [get_reference_count(load_json(path)) for path in paths]
    return counts


def print_reference_count_stats(parse):
    counts = get_all_reference_counts(parse)
    print('References per paper, for parse', parse)
    print('min ', min(counts))
    print('mean', mean(counts))
    print('max', max(counts))
    print('total', sum(counts))
    print('most occurring number of references, and the number of times it occurs')
    print(Counter(counts).most_common(10))
    print()


def check_reference_counts(n, parse):
    def print_info(paper, path):
        count = len(paper['bib_entries'])
        keys = list(paper['bib_entries'].keys())
        if len(keys) > 0:
            print(f'{count:>3} entries from {keys[0]} to {keys[-1]} \t {path} \t {keys}')

    paths = get_all_json_paths_for_parse(parse)
    paths_sample = random.choice(paths, size=n, replace=False)
    print('Sample for parse', parse)
    for path in paths_sample:
        print_info(load_json(path), path)


if __name__ == '__main__':
    # missing_titles('pdf_json')
    # missing_titles('pmc_json')

    print_reference_count_stats('pdf_json')
    print_reference_count_stats('pmc_json')
    # check_reference_counts(50, 'pdf_json')
    # check_reference_counts(10, 'pmc_json')
