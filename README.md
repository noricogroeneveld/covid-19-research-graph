# COVID-19 Research Graph
A project to create a graph based on the references in research related to COVID-19. Created for the COVID_19 Open Research Dataset Challenge on [Kaggle](https://www.kaggle.com/allen-institute-for-ai/CORD-19-research-challenge).

## Introduction
Papers reference other papers, and together they form a network.
This network contains various kinds of information.
For example, it can be used to find the most influential papers.
Or when looking at a specific paper, you can find papers that indirectly reference it or are indirectly referenced by it.
When looking at multiple papers, you can see how they are connected.
More complex information can also be extracted.

About two thirds of the papers in the dataset have json files that include the references.
This project extracts those references and saves the network.

The network is a graph with the papers as nodes and the references as directed edges.
The papers are identified by their titles, and the references are identified by a pair of titles.
The network can be imported in graph tools such as Neo4j or Gephi, which can be used to query various aspects of the network and visualise it.



## Getting started

### Data
The required data can be downloaded from the Kaggle Challenge on https://www.kaggle.com/allen-institute-for-ai/CORD-19-research-challenge.
The specter embeddings are not used and can be deleted.

### Graph creation
The graph can be loaded, cleaned, filtered and saved by running *creation/main.py*.

## Project directory overview
* *creation*: The steps of the pipeline, and some helper modules.
* *cypher*: Cypher queries to import the graph in Neo4j and get the most referenced papers. It also contains queries to create an example of the graph layout.
* *data*: Where the input and output data is located by default.
* *exploration*: Modules for exploratory data analysis and to create some statistics.
* *notebooks*:  Contains the notebook that was handed in for the Kaggle challenge. A live version can be found [here](https://www.kaggle.com/noricoxomnia/network-of-papers-and-references). 


## Using the graph export
[Neo4j](https://neo4j.com/) is a graph database, which is useful for data with many-to-many relationships.
It uses the Cypher Query Language and has a Browser to execute queries and view the results.
[Gephi](https://gephi.org/) is a program for the visualisation of graphs.
It has an interface that's not very complicated, though the number of options can be overwhelming.

### Neo4j
Create a database and put the csv files in the corresponding import folder.
Open Neo4j Browser and execute the first three Cypher queries in the *cypher/cypher_import.txt* file
You might have to change the filenames.

### Gephi import and visualisation
* From the tools menu, make sure the following plugins are installed: Circle Pack, SigmaJS exporter.
* From the file menu, open the csv file with nodes and follow the steps. Set the Graph Type to Directed.
* After that, open the csv file with edges. The only change you need to make is to append it to the current workspace instead of creating a new one.
* In the Statistics panel, run Modularity. There is no need to use weights, but set the Resolution 2 when you use many papers. The Modularity algorithm clusters the papers into highly connected communities.
* In the Appearance panel, set the node color to Partition using Modularity Class, optionally change the colors for some classes, and apply it.
* In the same panel, set the node size to Ranking using referenced, with size 5 to 50 (or 100 if using many papers), and apply it
* In the Layout panel, select Circle Pack Layout, set Hierarchy1 to Modularity Class and run it.
* In the Preview tab, set the Border With of Nodes to 0. Refresh to see the preview.
* From the file menu, export the graph as Sigma.js template. Enable *Include search* and *Group edges by direction*, and set the *Group Selector* To *Modularity class*. Make sure that the option *Replace node ids with numbers* is disabled. 
* To view the result, run *python -m http.server* in the folder with the Sigma.js template. It can also be uploaded, for example to Github Pages or Gitlab Pages. The nodes are smaller than in Gephi, but you can increase the *maxNodeSize* property in the *config.json* file. 


In the exported visualisation, you can select a paper and view its properties and connections. It is also possible to search for a paper or select a modularity cluster.
I created two exports.
One with the papers in the dataset that are referenced at least 5 times, and another one with all papers (including those outside the dataset) that are referenced at least twice.
Here are links to view the [first visualisation](https://noricogroeneveld.gitlab.io/covid-19-research-visualisation/) and the [larger second visualisation](https://noricogroeneveld.gitlab.io/covid-19-research-visualisation/full/).
The large visualisation has a large data file, so it takes a while to load and is slower.

Gephi can also be used to calculate properties of the graph, for example with the Pagerank algorithm.


## Next steps
### Improvements
* Better visualisation, for example cleaner properties.
* More insights from the network. There is research about research graph, which can help with that.
* Titles and references are assumed to be unique, but in the dataset they are not. This causes some information to be lost.
* Some titles clearly aren't a normal title for a paper.


### New features
* There is more information in the dataset. The most relevant are authors, laboratories, institutions and journals. They can be added to the network as new nodes and edges, or as extra properties for the current nodes. The publish time can be added as a property of the papers.
* The abstract can be added for papers that are referenced but are not in the dataset. These would have to be retrieved from somewhere.
* Perhaps there is more research that can be added, from other sources than the dataset, for example from [here](https://www.openaccess.nl/en/open-access-to-covid-19-and-related-research).
* To provide more information about the relationship, (part of) the sentence that a reference is used in can be extracted. The text and the location of a reference are known, so a simple version should be doable.




## Existing solutions
For COVID-19 research, there are two related solutions.
The [references-based atlas notebook](https://www.kaggle.com/mmoeller/a-references-based-atlas-of-covid-19-research) by Moritz Moeller extracts the references from PubMed and visualises them with Gephi.
On [covid.curiosity.ai](https://covid.curiosity.ai/) you can use a graph-based search engine with various exploration features.

There are also various projects related to research graphs in general, including: [Microsoft Academic Graph](https://www.microsoft.com/en-us/research/project/microsoft-academic-graph/), [OpenAIRE Research Graph](https://www.openaire.eu/blogs/the-openaire-research-graph), [ResearchGraph](https://researchgraph.org/), [Project Freya PID Graph](https://www.project-freya.eu/en/blogs/blogs/the-pid-graph) and the [Open Research Knowledge Graph](https://projects.tib.eu/orkg).
You can also find some research on the properties of research graphs and the information they contain.




## Full graph layout
This is a possible graph layout when extending the current layout.
### Nodes
* Paper
    * With properties, abstract and full text (including references)
    * With properties and abstract
    * Outside dataset (without abstract)
* Author
* Laboratory
* Institution
* Journal

### Edges
* Paper - references - paper
* Paper - published in - journal
* Paper - written by - author
* Author - working at - laboratory
* Laboratory - part of - institution

### Paths
There are many paths in the graph, including:
* Paper - shares author with - paper
* Author - worked together with - author
* Author - published in - journal
* Author - working at - institution
* Laboratory - published in - journal
* Laboratory - worked with - laboratory